package com.zy.printery.account.dao;

import com.zy.printery.comm.dto.CompanyDto;
import com.zy.printery.comm.quere.CompanyQuere;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CompanyMapper {

    String TABLE_NAME = "company";

    String TABLE_COLUMNS = "id,company_name,company_address,phone,company_brand,status,descs";

    String TABLE_COLUMNS_NID ="company_name,company_address,phone,company_brand,status,descs";

    String TABLE_RESULTS = "company_result";


    @SelectProvider(type=CompanyProvider.class,method = "findListPage")
    @Results(id=TABLE_RESULTS,value= {
            @Result(property = "id", column = "id"),
            @Result(property = "company_name", column = "companyName"),
            @Result(property = "company_address", column = "companyAddress"),
            @Result(property = "phone", column = "phone"),
            @Result(property = "company_brand", column = "companyBrand"),
            @Result(property = "status", column = "status"),
            @Result(property = "descs", column = "descs")
    })
     List<CompanyDto> findListPage(@Param("companyDto")CompanyQuere companyQuere);

    @SelectProvider(type=CompanyProvider.class,method = "findCount")
     Integer findCount(@Param("companyDto")CompanyQuere companyQuere);

    @Insert({
            "INSERT INTO "+TABLE_NAME ,
            "("+TABLE_COLUMNS_NID+")",
            "values(#{companyDto.companyName},#{companyDto.companyAddress},",
            "#{companyDto.phone},#{companyDto.companyBrand},#{companyDto.status},#{companyDto.descs}",
            ")"
    })
    Integer insert(@Param("companyDto") CompanyDto companyDto);

    @UpdateProvider(type = CompanyProvider.class,method = "update")
    Integer update(@Param("companyDto") CompanyDto companyDto);

    @Select({
            "SELECT " +TABLE_COLUMNS,
            "FROM " + TABLE_NAME,
            "WHERE id = #{id}"
    })
    @ResultMap(TABLE_RESULTS)
    CompanyDto findById(Integer id);

    @Select({
            "SELECT " +TABLE_COLUMNS_NID,
            "FROM " + TABLE_NAME,
            "WHERE id = #{id}"
    })

    @ResultMap(TABLE_RESULTS)
    CompanyDto findByBrand(String brand);
}
