package com.zy.printery.account.service;

import com.zy.printery.comm.dto.CompanyDto;
import com.zy.printery.comm.dto.PageDto;
import com.zy.printery.comm.quere.CompanyQuere;



public interface CompanyService {



    PageDto<CompanyDto> getPageList(CompanyQuere companyQuere);

    Integer saves(CompanyDto companyDto);

    CompanyDto findById(Integer id);

    CompanyDto findByBrand(String brand);
}
