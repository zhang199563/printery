package com.zy.printery.account.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.zy.printery.account.dao.CompanyMapper;
import com.zy.printery.account.service.CompanyService;
import com.zy.printery.comm.dto.CompanyDto;
import com.zy.printery.comm.dto.PageDto;
import com.zy.printery.comm.quere.CompanyQuere;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 *  必要得层级把参数给打印出来,方便以后排查问题
 *  zy
 */
@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService{

    private final CompanyMapper companyMapper;

    @Autowired
    public CompanyServiceImpl(CompanyMapper companyMapper){
        this.companyMapper = companyMapper;
    }


    @Override
    public PageDto<CompanyDto> getPageList(CompanyQuere companyQuere) {
        log.info("quere  company getPageList {}", JSONObject.toJSON(companyQuere));
        Integer count =companyMapper.findCount(companyQuere);
        if(count > 0){
            List<CompanyDto> companyDtoList = companyMapper.findListPage(companyQuere);
            return new PageDto(companyDtoList,count);
        }
        return  new PageDto(count);
    }

    @Override
    public Integer saves(CompanyDto companyDto) {
        log.info("insert company {}", JSONObject.toJSON(companyDto));
        if(StringUtils.isEmpty(companyDto.getId())){
            return companyMapper.insert(companyDto);
        }
        return companyMapper.update(companyDto);
    }


    @Override
    public CompanyDto findById(Integer id) {
        return companyMapper.findById(id);
    }

    @Override
    public CompanyDto findByBrand(String brand) {
        return companyMapper.findByBrand(brand);
    }
}
