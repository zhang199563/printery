package com.zy.printery.account.dao;


import com.zy.printery.comm.dto.CompanyDto;
import com.zy.printery.comm.quere.CompanyQuere;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.StringUtils;


public class CompanyProvider {

    public String findListPage(@Param("companyDto")CompanyQuere companyQuere){
        SQL sql = new SQL();
        sql.SELECT(CompanyMapper.TABLE_COLUMNS)
                .FROM(CompanyMapper.TABLE_NAME);
        setWhere(companyQuere,sql);
        sql.ORDER_BY(" id desc");
        return setLimit(companyQuere,sql);
    }


    public void setWhere(CompanyQuere companyQuere,SQL sql ){
        if(!StringUtils.isEmpty(companyQuere.getCompanyBrand())){
            sql.WHERE(" company_name = #{companyDto.companyBrand}");
        }

        if(!StringUtils.isEmpty(companyQuere.getCompanyName())){
            sql.WHERE("company_brand = #{companyDto.companyName}");
        }

        if(!StringUtils.isEmpty(companyQuere.getStatus())){
            sql.WHERE("status = #{companyDto.status}");
        }
    }

    private String setLimit(CompanyQuere companyQuere,SQL sql){
        if(!StringUtils.isEmpty(companyQuere.getStart()) &&!StringUtils.isEmpty(companyQuere.getEnd()) ){
           return sql.toString() +" limit "+companyQuere.getStart()+","+companyQuere.getEnd();
        }
        return sql.toString();
    }

    public String findCount(@Param("companyDto") CompanyQuere companyQuere){
        SQL sql = new SQL();
        sql.SELECT("count(1)")
                .FROM(CompanyMapper.TABLE_NAME);
        setWhere(companyQuere,sql);
        return sql.toString();
    }

    //Brand 不能改
    public String update(@Param("companyDto") CompanyDto companyDto){
        SQL sql = new SQL();
        sql.UPDATE(CompanyMapper.TABLE_NAME);
        if(!StringUtils.isEmpty(companyDto.getCompanyName())){
            sql.SET("company_name = #{companyDto.companyName}");
        }

        if(!StringUtils.isEmpty(companyDto.getCompanyAddress())){
            sql.SET("company_address = #{companyDto.companyAddress}");
        }

        if(!StringUtils.isEmpty(companyDto.getPhone())){
            sql.SET("phone = #{companyDto.phone}");
        }

        if(!StringUtils.isEmpty(companyDto.getStatus())){
            sql.SET("status = #{companyDto.status}");
        }

        if(!StringUtils.isEmpty(companyDto.getDescs())){
            sql.SET("descs = #{companyDto.descs}");
        }
        sql.WHERE("id = #{companyDto.id}");
        return sql.toString();
    }

}
