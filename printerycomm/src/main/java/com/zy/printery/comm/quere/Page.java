package com.zy.printery.comm.quere;

import com.zy.printery.comm.dto.enums.StatusEnum;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.time.Instant;

@Data
public class Page {

    private Integer index;

    private Integer size;

    private Integer start;

    private Integer end;

    private Instant startCreateTime;

    private Instant endCreateTime;

    private StatusEnum status;

    /**
     * 计算分页
     */
    public Page(){
        if(!StringUtils.isEmpty(index) &&  !StringUtils.isEmpty(size)){
            start = (index-1)*size;
            end = index * size;
        }
    }

}
