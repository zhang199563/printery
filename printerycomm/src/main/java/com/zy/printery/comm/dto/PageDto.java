package com.zy.printery.comm.dto;

import lombok.Data;

import java.util.List;


@Data
public class PageDto<T> {

    private List<T> pageList;

    private Integer pageCount;

    public PageDto(List<T> pageList,Integer pageCount){
        this.pageCount =pageCount;
        this.pageList = pageList;
    }

    public PageDto(Integer pageCount){
        this.pageCount =pageCount;
    }

}
