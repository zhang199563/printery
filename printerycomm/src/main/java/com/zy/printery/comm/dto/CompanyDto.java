package com.zy.printery.comm.dto;

import com.zy.printery.comm.quere.Page;
import lombok.Data;

@Data
public class CompanyDto  {

    private Integer id;

    private String companyName;

    private String companyAddress;

    private String phone;

    private String companyBrand;

    private String status;

    private String descs;
}
